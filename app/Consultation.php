<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    protected $table = 'consultation';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'mobile', 'email', 'city', 'sex',
        'age', 'howknowus', 'healthissue', 'skinissue', 'isundermedication',
        'howlongthisissue', 'forwhom', 'inviteforevents', 'tc_agree', 'status',
    ];
}
