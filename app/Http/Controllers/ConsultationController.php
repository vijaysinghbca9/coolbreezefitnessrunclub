<?php

namespace App\Http\Controllers;

use App\Consultation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ConsultationController extends Controller
{
    protected $redirectTo = '/';
    
    public function __construct()
    {
        //$this->middleware('guest');
    }
    
    public function showForm($type)
    {
        return view('bookconsultation', ['type'=>$type]);
    }
    
    public function register(Request $request, $type)
    {
        
        //Validates data
        $this->validator($request->all(), $type)->validate();
        
        //Register consultation
        $consultation = $this->create($request->all());
        
        //Notify user via mail
        $date = date("Y-m-d h:i:s A", time());
        $data = $request->all();
        $data['ConsultationNumber'] = $consultation->id;
        $data['Date'] = $date;
        $data['FirstName'] = $request['firstname'];
        $data['type'] = $type;
        try {
            $this->html_email($data);
        } catch (\Exception $ex) {
            $this->html_email($data);
        }
        
        //Redirects status.
        session()->flash('status', 'Congratulations '.$request['firstname']
            .'!!<br/><br/>Your consultation is booked successfully.<br/>A confirmation mail has been sent to your email address.');
        return redirect($this->redirectTo);
    }
    
    protected function validator(array $data, $type)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'mobile' => 'required|string|min:10|max:20',
            'email' => $type == 'p' ? 
                'required|string|email|max:50' : 
                'required|string|email|max:50|unique:consultation',
            'city' => 'required|string|max:255',
            'sex' => 'required|string|max:20',
            'age' => 'numeric|max:200',
            'howknowus' => 'string|max:255',
            'healthissue' => 'string|max:512',
            'skinissue' => 'string|max:512',
            'isundermedication' => 'string|max:255',
            'howlongthisissue' => 'string|max:255',
            'forwhom' => 'string|max:255',
            'inviteforevents' => 'string|max:50',
            'check' => 'required',
        ], [
            'email.unique'=>'You have already availed free consultation.'
        ]);
    }
    
    protected function create(array $data)
    {
        return Consultation::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'city' => $data['city'],
            'sex' => $data['sex'],
            'age' => $data['age'],
            'howknowus' => $data['howknowus'],
            'healthissue' => $data['healthissue'],
            'skinissue' => $data['skinissue'],
            'isundermedication' => $data['isundermedication'],
            'skinissue' => $data['skinissue'],
            'howlongthisissue' => $data['howlongthisissue'],
            'forwhom' => $data['forwhom'],
            'inviteforevents' => $data['inviteforevents'],
            'tc_agree' => '1',
            'status' => '10',
        ]);
    }
    
    public function status()
    {
        return view('status');
    }
    
    private function html_email(array $data)
    {
        Mail::send('email-confirmation-template', $data, function($message) use ($data) {
            $message->from(env('MAIL_USERNAME'),'I Am Healthy');
            $message->to($data['email'], $data['firstname']);
            $message->subject('Confirmation: Your consultation with I Am Healthy is successfully registered!');
        });
    }
}
