<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

	//Handles registration request for user
    public function register(Request $request)
    {
        session()->flash('action', 'register');

       //Validates data
        $this->validator($request->all())->validate();
		
       //Create user
        $user = $this->create($request->all());

        //Authenticates user
        //$this->guard()->login($user);
        
        //Notify user via mail
        $data = $request->all();
        $id = $user->id;
        $data['url'] = 'http://iamhealthy.asysinfo.com/account/activation/'.$id.'/'.Hash::make($id);
        $data['FirstName'] = $request['firstname'];

        try {
            $this->html_email($data);
        } catch (\Exception $ex) {
            $this->html_email($data);
        }

        session()->flash('status', 'Almost done!<br/><br/>You\'re almost done. Check your email.<br/>We just sent you an email with instructions on how to activate your account.');
        
       //Redirects users
        return redirect($this->redirectTo);
    }
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
			'lastname' => 'required|string|max:255',
			'mobile' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
			'password_confirmation' => 'required|string|min:6|same:password',
			'check' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
			'lastname' => $data['lastname'],
			'mobile' => $data['mobile'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
			'tc_agree' => '1',
			'status' => '20',
        ]);
    }
	
	//Get the guard to authenticate User
   protected function guard()
   {
       return Auth::guard('web');
   }
   
   public function activate($id, $hash)
   {
       try {
           //echo Hash::make($id);die;
           if (Hash::check($id, $hash)) {
               
               $count = User::where('id', $id)->where('status', 20)->update(['status'=>10]);
               if ($count > 0) {
                   $user = User::find($id);
                   
                   //Authenticates user
                   $this->guard()->login($user);
                   
                   //Redirects status.
                   session()->flash('status', 'Congratulations!!<br/><br/>Your account is activated successfully.<br/>Click <a href="http://iamhealthy.asysinfo.com/home">here</a> to access your account.');
               }
               else {
                   session()->flash('status', 'Your account is already activated.<br/>Click <a href="http://iamhealthy.asysinfo.com/home">here</a> to access your account.');
               }
           }
           else {
               session()->flash('status', 'Unknwon user. Cannot activate your account.<br/>Please, contact us at <a href="mailto:iamhealthy@asysinfo.com">iamhealthy@asysinfo.com</a>');
           }
       }
       catch (\Exception $ex) {
           session()->flash('status', 'An unknown error occured while activating your account.<br/>Please, try again after sometime.');
       }
       
       return redirect($this->redirectTo);
   }
   
   private function html_email(array $data)
   {
       Mail::send('email-activation-template', $data, function($message) use ($data) {
           $message->from(env('MAIL_USERNAME'),'I Am Healthy');
           $message->to($data['email'], $data['firstname']);
           $message->subject('I Am Healthy: Confirm your email address');
       });
   }
}
