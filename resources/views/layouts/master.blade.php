<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>I Am Healthy</title>
	<meta name="description" content="Get health related solutions & consultation from our certified nutritionists and wellness advisors. Our experts also provide high quality and completely natural supplements and ayurveda herbals. " />
	<meta name="keywords" content="health, skin, kids, nutrition, arthritis, cholesterol, diabetes, hair, ladies health, women nutrition, weight loss, weight gain, weight management, immunity" />
	<meta name="author" content="Asys Informations for I Am Healthy" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/favicons/apple-touch-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/favicons/apple-touch-icon-60x60.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('img/favicons/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ asset('img/favicons/favicon-16x16.png') }}" sizes="16x16">
	<link rel="manifest" href="{{ asset('img/favicons/manifest.json') }}">
	<link rel="shortcut icon" href="{{ asset('img/favicons/favicon.ico') }}">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="{{ asset('img/favicons/browserconfig.xml') }}">
	<meta name="theme-color" content="#ffffff">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/normalize.css') }}">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.css') }}">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.1.0/css/font-awesome.min.css') }}">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/eleganticons/et-icons.css') }}">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/cardio.css') }}">
</head>

<body>
	<div class="preloader">
		<img src="{{ asset('img/loader.gif') }}" alt="Preloader image">
	</div>
	<nav class="navbar">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="{{ asset('img/logo.png') }}"
					data-active-url="{{ asset('img/logo.png') }}" alt=""></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					@guest
    					<li><a id="aintro" href="#intro">Intro</a></li>
    					<li><a id="aservices" href="#services">Services</a></li>
<!--     				<li><a id="ateam" href="#team">Team</a></li> -->
						<li><a id="afree" href="#free">Free Consultation</a></li>
    					<li><a id="apricing" href="#pricing">Pricing</a></li>
    					<li><a id="aabout" href="#pricing">About</a></li>
    					<!-- Right Side Of Navbar -->
    					<!-- Authentication Links -->
    					<li><a href="#" data-toggle="modal" data-target="#modal1">Register</a></li>
    					<li><a id="alogin" href="#" data-toggle="modal" data-target="#modal2">Login</a></li>
					@else
						<li>
    						<div class="dropdown">
        						<button id="dLabel" class="form-control form-white dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        							My Account
        						</button>
        						<ul class="dropdown-menu animated fadeIn" role="menu" aria-labelledby="dLabel">
        							<li class="animated lightSpeedIn"><a href="#">Consultations</a></li>
        							<li class="animated lightSpeedIn"><a href="#">Profile</a></li>
        						</ul>
    						</div>
						</li>
    					<li>
                            <a class="form-control form-white" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"> {{ __('Logout') }} </a>
    						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
    					</li>
					@endguest
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div>
		@yield('content')
	</div>
	<div>
		@yield('guestcontent')
	</div>
	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup">
				<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
				<h3 class="white">{{ __('Register') }}</h3>
				<form action="{{ route('register') }}" class="popup-form" method="POST">
					@csrf
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }} form-white" placeholder="First Name" id="firstname" name="firstname" value="{{ old('firstname') }}" required autofocus />
						@if ($errors->has('firstname'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('firstname') }}</strong>
							</span>
						@endif
					</div>					
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }} form-white" placeholder="Last Name" id="lastname" name="lastname" value="{{ old('lastname') }}" required autofocus />
						@if ($errors->has('lastname'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('lastname') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-white" placeholder="Email Address" id="email" name="email" value="{{ old('email') }}" required />
						@if ($errors->has('email'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }} form-white" placeholder="Mobile Number" id="mobile" name="mobile" value="{{ old('mobile') }}" required autofocus />
						@if ($errors->has('mobile'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('mobile') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-white" placeholder="Password" id="password" name="password" required />
						@if ($errors->has('password'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input type="password" class="form-control form-white" placeholder="Confirm Password" id="password-confirm" name="password_confirmation" required />
					</div>
					<div class="form-group">
						<div class="checkbox-holder text-left">
							<div class="checkbox">
								<input type="checkbox" value="None" id="squaredOne" name="check" />
								<label for="squaredOne"><a href="{{ url('/termsandconditions') }}"><span>I Agree to the <strong>Terms &amp; Conditions</strong></span></a></label>
							</div>
						</div>
							@if ($errors->has('check'))
								<span class="invalid-feedback">
									<strong>Please accept terms & conditions</strong>
								</span>
							@endif
					</div>
					<button type="submit" class="btn btn-submit" id="btnregister" name="submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup">
				<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
				<h3 class="white">{{ __('Login') }}</h3>
				<form action="{{ route('login') }}" class="popup-form" method="POST">
					@csrf
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-white" placeholder="Email Address" id="email" name="email" value="{{ old('email') }}" required />
						@if ($errors->has('email'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-white" placeholder="Password" id="password" name="password" required />
						@if ($errors->has('password'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
					<div class="checkbox-holder text-left">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
								<label for="remember"><span>{{ __('Remember Me') }}</strong></span></label>
							</label>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-submit" id="btnlogin">
							{{ __('Login') }}
						</button>

						<a class="btn btn-link" href="{{ route('password.request') }}">
							{{ __('Forgot Your Password?') }}
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<footer>
		<div class="container">
			@guest
			<div class="row">
				<div id="free" class="col-sm-6 text-center-mobile">
					<h3 class="white">Reserve a First Free Health Consultation!</h3>
					<h5 class="light regular light-white">Shape your body and improve your health.</h5>
					<a href="{{ url('/consultation/form/f') }}" class="btn btn-blue ripple trial-button">Free Consultation</a></li>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Consultation Hours <span class="open-blink"></span></h3>
					<div class="row opening-hours">
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Mon - Fri</h5>
							<h3 class="regular white">9:00 - 23:00</h3>
						</div>
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Sat - Sun</h5>
							<h3 class="regular white">10:00 - 23:00</h3>
						</div>
					</div>
				</div>
			</div>
			@endguest
			<div class="row bottom-footer text-center-mobile">
				<div class="col-sm-8">
					<p>&copy; 2018 All Rights Reserved. Powered by <a href="http://asysinfo.com/">Asys Informations</a> exclusively for <a href="http://iamhealthy.asysinfo.com/">I Am Healthy</a></p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					<ul class="social-footer">
						<li><a href="http://www.facebook.com/yesiamhealthy"><i class="fa fa-facebook"></i></a></li>
						<li><a href="http://www.twitter.com/codrops"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://plus.google.com/101095823814290637419"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup">
				<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
    			<div class="header-text">
    				<h4 class="white">{{ __('Status') }}</h4>
    				<div class="form-group">
                        @if (session('status'))
                            <div class="alert alert-success">
                                <strong>{!! session('status') !!}</strong>
                            </div>
                        @endif
    				</div>
    			</div>
    		</div>
		</div>
	</div>
	<!-- Scripts -->
	<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/wow.min.js') }}"></script>
	<script src="{{ asset('js/typewriter.js') }}"></script>
	<script src="{{ asset('js/jquery.onepagenav.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>
	<script src="{{ asset('js/app.js') }}" defer></script>
	<script type="text/javascript">
		@if ((session('action') == 'register') && count($errors) > 0)
			$('#modal1').modal('show');
		@elseif ((session('action') == 'login') && count($errors) > 0)
			$('#modal2').modal('show');
		@endif

		@if (session('status'))
			$('#modalStatus').modal('show');
		@else
			$('#modalStatus').modal('hide');
		@endif
	</script>
</body>

</html>
