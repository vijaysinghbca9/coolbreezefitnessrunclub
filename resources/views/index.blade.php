@extends('layouts.master')
@section('guestcontent')
	@guest
	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 class="light white">Take care of your body.</h3>
							<h1 class="white typed">It's the only place you have to live.</h1>
							<span class="typed-cursor">|</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row intro-tables">
				<div class="col-md-4">
					<div class="intro-table intro-table-first">
						<h5 class="white heading">Today's Schedule</h5>
						<div class="owl-carousel owl-schedule bottom">
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Early Exercise</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">06:30 - 08:30</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Muscle Building</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">06:30 - 08:30</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Cardio</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">06:30 - 08:30</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Early Exercise</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Muscle Building</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Cardio</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Early Exercise</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Muscle Building</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Cardio</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-table intro-table-hover">
						<h5 class="white heading hide-hover">Premium Membership</h5>
						<div class="bottom">
							<h4 class="white heading small-heading no-margin regular">Register Today</h4>
							<h4 class="white heading small-pt">20% Discount</h4>
							<a href="{{ url('/consultation/form/p') }}" class="btn btn-white-fill expand">Register</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-table intro-table-third">
						<h5 class="white heading">Happy Clients</h5>
						<div class="owl-testimonials bottom">
							<div class="item">
								<h4 class="white heading content">Incredible transformation and I feel so healthy!</h4>
								<h5 class="white heading light author">Farheen Naaz</h5>
							</div>
							<div class="item">
								<h4 class="white heading content">I couldn't be more happy with the results!</h4>
								<h5 class="white heading light author">Sumesh Joseph</h5>
							</div>
							<div class="item">
								<h4 class="white heading content">I can't believe how much better I feel!</h4>
								<h5 class="white heading light author">Kusum Singh</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="services" class="section section-padded">
		<div class="container">
			<div class="row text-center title">
				<h2>Services</h2>
				<h4 class="light muted">Achieve the best results with our customized solutions in accordance to your body type!</h4>
			</div>
			<div class="row services">
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Kids Health & Nutrition</h4>
						<p class="description">Children's unique nutrient needs, Immunity, Energy, Frequent Sickness, Body Growth, etc</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Weight Management</h4>
						<p class="description">Weight gain or loss without compromising food, Less exercise, Self Motivation, Guaranteed Results.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Beauty & Skin Care</h4>
						<p class="description">Oily &amp; Dry Skin, Flakiness, Acne, Pimples, Wrinkles, Ageging, Dark Spots, Dark Circles, Tanning, Facial, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Women Health</h4>
						<p class="description">Weak Immune, Frequent Sickness, Fatigue, Body Pain, Lack of energy, Anemia, Arthritis, Nutrition, Beauty, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Hair Care</h4>
						<p class="description">Hair Loss, Dry Hair, Less Hair, Dandruff, Dry or Weak Scalp, Brittle Hairs, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Arthritis</h4>
						<p class="description">Joint Pain, Painful &amp; Swollen Knees, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Heart Health</h4>
						<p class="description">Cholesterol, Blood Pressure, Haemoglobin, Heart Strength, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Sports Nutrition</h4>
						<p class="description">Whey Protein, Energy Drinks, Diet Planning, Excerise, Muscle Building, Marathon Guidance, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Healthy Cooking</h4>
						<p class="description">Zero Oil Cooking, Nutrients Rich Food, Less LPG/Electricity Usage, Less Cooking Time, Amazing Taste, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Organic Supplements</h4>
						<p class="description">Proteins, Vitamins, Omega-3, Iron, Calcium, Fiber, etc from Nutilite, XS, etc</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Ayurveda Herbals</h4>
						<p class="description">Nutrilite Tulsi, Triphla, Brahmi &amp; Ashwagandha, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<img src="{{ asset('img/icons/weight-blue.png') }}" alt="" class="icon">
						</div>
						<h4 class="heading">Expert Consultation</h4>
						<p class="description">Solution to specific problem, Diet Planning for kids and adult, Long-term support, Preventive Measures, Experienced Advisors, etc.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="cut cut-bottom"></div>
	</section>
<!-- <section id="team" class="section gray-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Team</h2>
				<h4 class="light muted">We're a dream team!</h4>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="team text-center">
						<div class="cover" style="background:url('img/team/team-cover1.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">$â‚¹850.00</h3>
								<h5 class="light light-white">Per 6 months</h5>
							</div>
						</div>
						<img src="{{ asset('img/team/team3.jpg') }}" alt="Team Image" class="avatar">
						<div class="title">
							<h4>Joe</h4>
							<h5 class="muted regular">Wellness Advisor</h5>
						</div>
						<button data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill">Fix Appointment Now</button>
					</div>
				</div>
				<div class="col-md-4">
					<div class="team text-center">
						<div class="cover" style="background:url('img/team/team-cover2.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">â‚¹850.00 - â‚¹1050.00</h3>
								<h5 class="light light-white">Per 6 months</h5>
							</div>
						</div>
						<img src="{{ asset('img/team/team1.jpg') }}" alt="Team Image" class="avatar">
						<div class="title">
							<h4>Minati</h4>
							<h5 class="muted regular">Nutritionist<br/>&amp;<br/>Skin Care Expert</h5>
						</div>
						<a href="#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Fix Appointment Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="team text-center">
						<div class="cover" style="background:url('img/team/team-cover3.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">â‚¹850.00 - â‚¹1500.00</h3>
								<h5 class="light light-white">Per 6 months</h5>
							</div>
						</div>
						<img src="{{ asset('img/team/team2.jpg') }}" alt="Team Image" class="avatar">
						<div class="title">
							<h4>Vijay Singh</h4>
							<h5 class="muted regular">Consultant<br/>&amp;<br/>Weight Management Expert</h5>
						</div>
						<a href="#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Fix Appointment Now</a>
					</div>
				</div>
			</div>
		</div>
	</section>-->
	<section id="pricing" class="section">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top white">Pricing</h2>
				<h4 class="light white">New launch pricing plan and register today!</h4>
			</div>
			<div class="row no-margin">
				<div class="col-md-7 no-padding col-md-offset-5 pricings text-center">
					<div class="pricing">
						<div class="box-main active" data-img="{{ asset('img/pricing1.jpg') }}">
							<h4 class="white">Consultation Charges</h4>
							<h4 class="white regular light"><strike>₹1000.00</strike><br/>₹850.00 <span class="small-font">/ 6 months</span></h4>
							<a href="#" data-toggle="modal" data-target="#modal1" class="btn btn-white-fill">Register Now</a>
							<i class="info-icon icon_question"></i>
						</div>
						<div class="box-second active">
							<ul class="white-list text-left">
								<li>One Personal Consultant</li>
								<li>One Time Consultation Fee</li>
								<li>Referral Gifts & Discounts</li>
								<li>Free Ongoing Support</li>
								<li>Special Discounts*</li>
							</ul>
						</div>
					</div>
					<div class="pricing">
						<div class="box-main" data-img="{{ asset('img/pricing2.jpg') }}">
							<h4 class="white">Cardio Training</h4>
							<h4 class="white regular light">₹120.00 <span class="small-font">/ session</span></h4>
							<a href="#" data-toggle="modal" data-target="#modal1" class="btn btn-white-fill">Sign Up Now</a>
							<i class="info-icon icon_question"></i>
						</div>
						<div class="box-second">
							<ul class="white-list text-left">
								<li>One Personal Consultant</li>
								<li>One Time Consultation Fee</li>
								<li>Referral Gifts & Discounts</li>
								<li>Free Ongoing Support</li>
								<li>Special Discounts*</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-padded blue-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="owl-twitter owl-carousel">
						<div class="item text-center">
							<i class="icon fa fa-twitter"></i>
							<h4 class="white light">To enjoy the glow of good health, you must exercise.</h4>
							<h4 class="light-white light">#health #training #exercise</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-twitter"></i>
							<h4 class="white light">Follow a diet which provides you nutrients in the required amount. This can really help you in staying fit. Thus a balanced diet is always recommended.</h4>
							<h4 class="light-white light">#fitness #diet #nutrients</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-twitter"></i>
							<h4 class="white light">It is always advised to consume fresh fruits and vegetables. These fulfil the body’s requirement of nutrients and roughage.</h4>
							<h4 class="light-white light">#fruits #vegetables #roughage</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-twitter"></i>
							<h4 class="white light">Antioxidants work against the functioning of free radicals. Vitamins C and e function as free radical remover. So its intake can help in maintaining a good health.</h4>
							<h4 class="light-white light">#freeradicals #vitaminC #antioxidants</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-twitter"></i>
							<h4 class="white light">A sound sleep of at least 7-8 hours can do wonders for your health. There is no substitute for a good sleep.</h4>
							<h4 class="light-white light">#sleep #health #exercise</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endguest
@endsection