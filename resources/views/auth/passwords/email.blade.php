@extends('layouts.master')

@section('content')
@guest
<header id="intro">
	<div class="container">
		<div class="table center">
			<div class="header-text">
				<h4 class="white">{{ __('Reset Password') }}</h4>
				<div class="form-group">
					<form method="POST" action="{{ route('password.email') }}">
                        @csrf
						<div class="form-group">
                            <div class="">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Address" name="email" value="{{ old('email') }}" required>
								@if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-submit">
								{{ __('Send Password Reset Link') }}
							</button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</header>
@endguest
@endsection
