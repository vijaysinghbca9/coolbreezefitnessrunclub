@extends('layouts.master')

@section('content')
<header id="intro" style="padding-top: 170px;">
	<div class="modal-dialog modal-popup">
		<div class="table center">
			<div class="header-text">
				<h3 class="white">{{ __('Login') }}</h3>
				<form action="{{ route('login') }}" class="popup-form" method="POST">
					@csrf
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-white" placeholder="Email Address" id="email" name="email" value="{{ old('email') }}" required />
						@if ($errors->has('email'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-white" placeholder="Password" id="password" name="password" required />
						@if ($errors->has('password'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
					<div class="checkbox-holder text-left">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
								<label for="remember"><span><strong>{{ __('Remember Me') }}</strong></span></label>
							</label>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-submit" id="btnlogin">
							{{ __('Login') }}
						</button>

						<a class="btn btn-link" href="{{ route('password.request') }}">
							{{ __('Forgot Your Password?') }}
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</header>
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript">
    $('#aintro').css({ 'display': 'none'});
    $('#aservices').css({ 'display': 'none'});
    $('#apricing').css({ 'display': 'none'});
	$('#alogin').css({ 'display': 'none'});
</script>
@endsection
