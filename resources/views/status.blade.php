@extends('layouts.master')

@section('content')
<header id="intro">
	<div class="container">
		<div class="table center">
			<div class="header-text">
				<h4 class="blue">{{ __('Status') }}</h4>
				<div class="form-group">
                    @if (session('status'))
                        <div class="alert alert-success">
                            <strong>{!! session('status') !!}</strong>
                        </div>
                    @endif
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
