@extends('layouts.master')
@section('content')
<header id="intro" style="padding-top: 200px;" >
	<div class="container">
    	<h2 class="blue" align="center">Terms &amp; Conditions</h2><br/><br/>
    	<h3 style="color: #8a8e91;">The Consultation</h3><br/>
    	<p>I Am Healthy is not a substitute for professional medical advice and/or treatment and clients are responsible for contacting their GP about any health concerns.  I Am Healthy is not intended to replace the advice of medical professionals.</p>
        <p>Nutritional advice will be tailored to support medically established, diagnosed conditions and/or health concerns identified and agreed between the Nutritional Therapist and client. Nutritional Therapists are not permitted to diagnose, or claim to treat medical conditions.</p>
        <p>Supplements and / or tests may be recommended as part of the client’s nutritional programme, a commission may be received by I Am Healthy on these products or services</p>
        <p>Standards of professional practice in I Am Healthy are governed by the BANT code of ethics and practice.  The client will be required to sign full terms of agreement prior to the first consultation.</p>
        <p>If the client is receiving treatment from their GP, other medical provider or complementary therapist they should advise them of any nutritional strategy provided by the nutritional therapist. This is necessary in case of any possible reaction between medication and the nutritional programme. I Am Healthy can write to your GP at your request.</p>
        <p>The client must inform the nutritional therapist of any medical diagnosis, medication, herbal medicine or food supplements they are taking as this may affect the nutritional programme.</p>
        <p>The client should contact the nutritional therapist if unclear about any areas of the agreed nutritional programme including supplementation and timeframes and if they wish to continue a dietary supplement or programme longer than the specified period.</p>
        <p>Clients are advised to report any concern about their programme promptly to the nutritional therapist for discussion and action.</p>
     </div>       
</header>
<div class="container">
    <h3>Business Practise</h3>
    <p>Payment: Payment can be made by cash or cheque at the time of the first consultation</p>
    <p>Cancellations: We request that you give 24 hours’ notice of cancellation.  Less than 24 hours’ notice of cancellation of an appointment may incur a 100% cancellation fee.</p>
    <p>Data Protection: Information provided for the purpose of the nutrition consultation will be stored for this purpose only and for legal requirements. It will not be passed to any third party and will be treated within the terms of the Act.  If a situation should arise where we feel a doctor should be informed we will discuss this with you and seek your permission before revealing any information to them.  Any client with “red flag” signs or symptoms will always be referred to their medical professional.</p>
    <br/>
	<h3>Use of Cookies</h3>
    <p>Like most interactive web sites this website uses cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate partners may also use cookies. By using our website you agree to have cookies added to your browser software by us or our partners.</p>
    <br/>
	<h3>The Website</h3>
    <p>Our website and its contents are provided for general information purposes only and nothing on this website or in its contents is intended to provide medical or other professional advice. I Am Healthy cannot be held responsible for any claims relating to issues that are deemed have possibly arisen from reading any material on this website, including any websites linked from this website.  The content provided is of personal opinion only.  No claims are made that the content is suitable or applicable to all persons. If you wish to find out more about the information in the materials published, please <a href="mailto:iamhealthy@asysinfo.com">contact us by email</a>.</p>
    <p>We are not responsible or liable for any matter relating to you or any third parties accessing or using this website and its contents.</p>
    <p>We do not endorse nor are we responsible for the contents of websites operated by others that link to this website or that are accessible from it</p>
    <p>This website is for your own private use. By accessing this website, you agree:</p>
    <ul>
        <li>not to use this website or its content in contravention of any regulation or legislation</li>
        <li>that, with the exception of copyright belonging to third parties and unless otherwise stated, copyright in the pages of this website and all other material available through it belongs to I Am Healthy.</li>
        <li>That we, our suppliers or third parties who have granted us permission to reproduce their material on this website, own all trademarks, copyright and all other intellectual property rights in the content on this website</li>
        <li>not to copy, amend, reproduce or distribute the images to third parties, other than
            <ul>
                <li>You may print or save copies of the content of this website for your own personal use and you may provide copies to others for information purposes, on the basis that
                    <ul>
                        <li>you do so on an occasional basis and free of charge</li>
                        <li>the copies are not tampered with in any way</li>
                    </ul>
                </li>
                <li>Any other reproduction, transmission and storing of all or part of this website and the materials available through it, in any medium, without the written permission of I Am Healthy, is prohibited.</li>
            </ul>
        </li>
    </ul>
</div>
<div class="container">
	<p><strong>Your use of this site is expressly conditioned on your acceptance of the following terms and conditions. By using this site, you signify your assent to these terms and conditions.</strong> If you do not agree with any part of the following terms and conditions, please do not use this site.</p>
	<br/>
	<p> The term 'us' or 'we' refers to the owner of the website, I Am Healthy (India) Ltd and the 'website' refers to the site at url: <a href="http://iamhealthy.asysinfo.com/">www.iamhealthy.asysinfo.com</a> . Office of I Am Healthy (India) Ltd is #5, Tirumala Nilaya, Room #17, 1st Main, 6th Cross, 1st Block Kormangala, Bangalore - 560034. The term 'you' refers to the user or viewer of our Website.</p>
	<br/>
	<p> <strong>Governing Law.</strong> This agreement shall be governed by and construed in accordance with the laws of India. The Indian courts will have non-exclusive jurisdiction over any claim arising from, or related to, a visit to our Website although we retain the right to bring proceedings against you for breach of these conditions in your country of residence or any other relevant country.</p>
</div>
<div class="container">
	<h3> Content</h3>
	<ul>
		<li>The content of the pages of this Website is for your general information and use only. It is subject to change without notice. The information is provided by I Am Healthy and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the Website or the information, products, services, or related graphics contained on the Website for any purpose.</li>
		<li>It shall be your own responsibility to ensure that any products, services or information available through this Website meet your specific requirements. This Website and related social networking feeds may contain general information about nutrition, well-being, medical conditions and treatments.</li>
		<li>If you think you may be suffering from any medical condition or have any concerns at all about your physical or emotional health or well-being, please refer yourself immediately to a medical professional. The information contained on this Website and related social networking feeds is not intended to be an alternative to medical advice from your doctor or other professional healthcare provider, and should not be used as such. If you have any specific questions about any medical matter you should consult your doctor or other professional healthcare provider. You should never delay seeking medical advice, disregard medical advice, or discontinue medications or medical treatment because of any information disseminated or linked to by I Am Healthy.</li>
		<li>Commentary and other materials posted on our Website are not intended to amount to advice on which reliance should be placed. We therefore disclaim all liability and responsibility arising from any reliance placed on such materials by your visit to our Website, or by anyone who may be informed of any of its contents.</li>
		<li>From time to time, this Website may also include links to other Websites which are not under the control of I Am Healthy. We have no control over the nature, content and availability of those sites. These links are provided for your convenience to provide further information. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them. We have no responsibility for the content of the linked Website(s).</li>
	</ul>
	<h3>Limitation of Liability</h3>
	<ul>
		<li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this Website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors. We expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
		<li>I Am Healthy assumes no responsibility, and shall not be liable for any damages to, or viruses that may infect your computer equipment or other property on account of your access to, use of, or browsing in this Website or your downloading of any materials, data, text, images, video, or audio from this site. At I Am Healthy's sole discretion, in addition to any other rights or remedies available to I Am Healthy and without any liability whatsoever, I Am Healthy at any time and without notice may terminate or restrict your access to any component of this site.</li>
		<li>Every effort is made to keep the Website up and running smoothly. However, I Am Healthy takes no responsibility for, and will not be liable for, the Website being temporarily unavailable due to technical issues beyond our control.</li>
		<li>Any liability for any direct, indirect or consequential loss or damage incurred by any user in connection with our Website or in connection with the use, inability to use, or results of the use of our Website, any websites linked to it and any materials posted on it, including:
	<ul>
		<li>loss of income or revenue;</li>
		<li>loss of business;</li>
		<li>loss of profits or contracts;</li>
		<li>loss of data;</li>
		<li>loss of goodwill;</li>
		<li>wasted management or office time; and<br>
		whether caused by tort (including negligence), breach of contract or otherwise, even if foreseeable, provided that this condition shall not prevent claims for loss of or damage to your tangible property or any other claims for direct financial loss that are not excluded by any of the categories set out above.<br>
		This does not affect our liability for death or personal injury arising from our negligence, nor our liability for fraudulent misrepresentation or misrepresentation as to a fundamental matter, nor any other liability which cannot be excluded or limited under applicable law.</li>
	</ul>
	</li>
	</ul>
	<h3>Subscriptions</h3>
	<ul>
		<li> By signing up for the I Am Healthy website, you warrant that you are either a resident of India, or if resident in another country, agree to the use of the Website being governed by Indian law.</li>
		<li>If you are 12 years or younger, you will need to get your parent or guardian to sign up on your behalf.</li>
		<li>You will be offered the option to subscribe to the Website on your personal profile page, but are under no obligation to do so.</li>
		<li>If you choose not to subscribe, your data will be saved securely and available for you to access if you decide to subscribe to the Website at any point in the future. However you will not be able to view existing data nor to add any more data whilst you are not subscribed to the Website. The only exception to this is that if there was a read-only link generated to your Nourish calendar it will remain live.</li>
		<li>Subscription prices will be clearly displayed throughout the Website and at the payment point, but we reserve the right to change those subscription prices at any time.</li>
		<li>If you choose to subscribe and make a payment for an agreed period of time, you will need to read through and accept our <a href="http://iamhealthy.asysinfo.com/termsandconditions#subscriptionterms">subscription terms</a> (see below), including payment terms. Once you have subscribed, the Website will inform you of when that period will be complete by displaying that date on your personal profile page.</li>
		<li>Any subscription payment made through the Website is non-refundable regardless of how many times you access the Website, nor how useful or otherwise you consider the Website to be.</li>
	</ul>
	<h3> Personal Data &amp; Privacy</h3>
	<ul>
		<li>We process data about you only in the way described in these terms. By using our Website you consent to our processing of your data as described in these terms and you warrant that all data provided by you is accurate.</li>
		<li>I Am Healthy will protect users' personal details using recognised secure data protection tools and practices. All data we hold will be anonymised. However, we will be forced to consider disclosure of certain information to legal authorities where there are strong grounds for believing that not doing so will result in harm to research participants or others, or any form of illegal activity, as outlined above.</li>
		<li>All research undertaken by or in collaboration with I Am Healthy uses only anonymous data. For administration purposes only, you will be assigned a number on our database as you join the site, and all data that you enter on the Website will be supplied linked to that number only, if it is shared in the context of collaborative research. Your name, email address and contact details will not be linked to that research data report and therefore not shared with any third parties.</li>
		<li>Your personal data will not be shared with any third parties for advertising or marketing purposes.</li>
		<li>If you leave all or part of your Personal Profile blank, your anonymous data will not be included in any research related to the questions contained in the profile that you have chosen not to answer; in this way, you can choose to opt out of some specific research projects.</li>
		<li>All information entered on the I Am Healthy Website will be tracked using keywords to search for general trends as outlined in the Cookie section above (for example, what is the most common word that users are entering in their Happiness Calendar?). It is not possible for you to opt out of this anonymous data collection, except by choosing not to use the I Am Healthy Website.</li>
	</ul>
	<h3>Copyright notice</h3>
	<p> I Am Healthy Limited is the owner or licensee of all intellectual property rights in our Website and in the material posted on it. These works are protected by copyright laws and treaties around the world. All such rights are reserved. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Any redistribution, reproduction, resale, transference or modification of part or all of the contents in any form is prohibited other than the following:</p>
	<ul>
		<li>you may print or download to a local hard disk extracts for your personal and non-commercial use only</li>
		<li>you may copy the content to individual third parties for their personal use, but only if you acknowledge the Website as the source of the material</li>
		<li>you may share links to your own personal information with whoever you choose, but only if you acknowledge the Website as the source of the material</li>
		<li>if you are a professional who chooses to recommend the Website to support the work that you do, you must not create the impression that you are employed by or are in any way connected to I Am Healthy, unless we have entered into an agreed collaboration. You must credit I Am Healthy as the source of any information that you use from the Website.</li>
	</ul>
	<p>You may not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. You may not, except with our express written permission, distribute or commercially exploit the content. Nor may you transmit it or store it in any other Website or other form of electronic retrieval system. Unauthorised use of this Website may give rise to a claim for damages and/or be a criminal offence. If you wish to collaborate with us, we would welcome the opportunity to discuss your idea; please contact us via the links on the Contact page of this Website.<br>
	We reserve the right to change these Terms and Conditions from time to time. This may be for reasons including legal or administrative requirements, or to correct an inaccurate statement. We will provide notice of the change to you via the Website where we deem this necessary. We encourage you to review these Terms periodically to check you understand the latest version.</p>
	<br /><br />
	<h3 id="subscriptionterms">I Am Healthy Website Subscriber Terms</h3>
	<p><strong>Your subscription to our services on this Website is expressly conditioned on your acceptance of the following terms and conditions.</strong></p>
	<p> Please read these terms and conditions carefully and make sure that you understand them, before subscribing to any services from our Website. If you are under 12 you can only subscribe together with your parent or guardian. If you are under 16 you must let your parent or guardian know about our Privacy Policy (see clause 6) before you subscribe to our Services. You should understand that by subscribing to our services, you agree to be bound by these terms and conditions. If you do not agree with any part of the following terms and conditions, please do not subscribe to this Website.</p>
	<p> You should print a copy of these terms and conditions for future reference.</p>
	<p> The term 'us' or 'we' refers to the owner of the website, I Am Healthy (India) Ltd and the 'Website' refers to <a href="http://iamhealthy.asysinfo.com/">www.iamhealthy.asysinfo.com</a> . The registered offices of I Am Healthy (India) Ltd is #5, Tirumala Nilaya, Room #17, 1st Main, 6th Cross, 1st Block Kormangala, Bangalore - 560034. The term 'you' refers to the subscriber to our Website services.</p>
	<h3> Our terms</h3>
	<ol>
		<li><b>Definitions</b>
		<ol>
			<li> When the following words with capital letters are used in these Terms, this is what they will mean:
				<ol>
					<li><strong>Event Outside Our Control:</strong> is defined in clause 9.2;</li>
					<li><strong>Order:</strong> your order for the Services;</li>
					<li><strong>Services:</strong> the subscriber services that we are providing to you as set out in the Order;</li>
					<li><strong>Subscription Period:</strong> the period during which you are a Subscriber to our Services; and</li>
					<li><strong>Terms:</strong> the terms and conditions set out in this document.</li>
				</ol>
			</li>
			<li>When we use the words "writing" or "written" in these Terms, this will include e-mail unless we say otherwise.</li>
		</ol>
		</li>
		<p></p>
		<li><b> Our contract with you</b>
		<ol>
			<li> These are the terms and conditions on which we supply Services to you.</li>
			<li> Please ensure that you read these Terms carefully, and check that the details on the Order page of our Website and in these Terms are complete and accurate, before you submit the Order. If you think that there is a mistake, please contact us to discuss, and please make sure that you ask us to confirm any changes in writing to avoid any confusion between you and us.</li>
			<li> We consider that these Terms, our Website Terms of Use and the Order constitute the whole agreement between you and us.</li>
			<li> When you submit the Order to us through our Website, this does not mean we have accepted your order for Services. Our acceptance of the Order will take place as described in clause 2.5. If we are unable to supply you with the Services, we will inform you of this and we will not process the Order.</li>
			<li> These Terms will become binding on you and us when we issue you with a written acceptance of an Order, at which point a contract will come into existence between you and us.</li>
			<li> If any of these Terms conflict with any term of the Order, the Order will take priority. If any of these Terms conflict with any term of the Website Terms of Use, these terms will take priority.</li>
		</ol>
		</li>
		<p></p>
		<li><b> Changes to order or terms
		</b>
		<ol>
			<li> We may revise these Terms from time to time in the following circumstances:
			<ol>
				<li> changes in how we accept payment from you;</li>
				<li> changes in relevant laws and regulatory requirements; or</li>
				<li> other circumstances.</li>
			</ol>
			</li>
			<li> If we have to revise these Terms under clause 3.1, we will give you at least one month's written notice of any changes to these Terms before they take effect. You can choose to cancel the contract in accordance with clause 10.</li>
			<li> You may make a change to the Order for Services at any time before the start of your Subscription Period by contacting us. Where this means a change in the total price of the Services, we will notify you of the amended price in writing. You can choose to cancel the Order in accordance with clause 10.1 in these circumstances.</li>
			<li> If you wish to cancel an Order before it has been fulfilled, please see your right to do so in clause 10.</li>
		</ol>
		</li>
		<p></p>
		<li><b> Providing services
		</b>
		<ol>
			<li> We will supply the Services to you from the date agreed between us in writing until the end of the Subscription Period set out in the Order.</li>
			<li> We will make every effort to provide the Services at all times during your Subscription Period. However, there may be delays due to an Event Outside Our Control. See clause 9 for our responsibilities when an Event Outside Our Control happens.</li>
			<li> We may have to suspend the Website if we have to deal with technical problems. We will contact you to let you know in advance where this occurs, unless the problem is urgent or an emergency.</li>
		</ol>
		</li>
		<p></p>
		<li><b> Price and payment</b>
		<ol>
			<li> The price of the Services will be set out on the Website at the time we confirm your Order. Our prices may change at any time, but price changes will not affect Orders that we have already confirmed with you.</li>
			<li> These prices include VAT. However, if the rate of VAT changes between the date of the Order and the date of delivery or performance, we will adjust the rate of VAT that you pay, unless you have already paid for the Services in full before the change in the rate of VAT takes effect,</li>
			<li> We will charge you in advance for the subscription Services at the time we accept your Order.</li>
			<!-- <li> Payment for all Services must be in advance of the Subscription Period and by credit or debit card and will be processed by Secure Trading Limited or WorldPay (India) Limited. We accept payment with all major credit and debit cards accepted by Secure Trading Limited and WorldPay (India) Limited. We will not charge your credit or debit card until we accept your Order.</li>
			<li> Any subscription payment made through the Website is, subject to clause 10, non-refundable regardless of how many times you access the Website, nor how useful or otherwise you consider the Website to be.</li> -->
		</ol>
		</li>
		<p></p>
		<li><b> Privacy policy - Information we may collect from you</b>
		<ol>
			<li> When you place an Order for subscription online at our Website, you give us your consent to process and use such submitted data in accordance with these terms. For the purposes of the Data Protection Act 1998, the data controller is I Am Healthy (India) Limited whose address is stated at the beginning of these terms.</li>
			<li> I Am Healthy does not wish to collect personal information from anyone under the age of twelve (12) without the consent of their parent or guardian. If you are under the age of twelve (12), please ask your parent or guardian to register on your behalf.</li>
			<li> When you visit our Website or place an order through our Website, we may collect and process the following data about you:
			<ol>
				<li> Information that you provide by filling in forms on our Website. This includes information provided at the time of registering to use our Website, or subscribing to our Service. We may also ask you for information if you report a problem with our Website. </li>
				<li> Information that you provide by entering your personal thoughts and data under the 'happiness', 'shift' and 'nourish' headings on our Website.</li>
				<li> If you contact us, we may keep a record of that correspondence.</li>
				<li> We may also ask you to complete surveys, questionnaires and profile questions that we use for research purposes, although you do not have to respond to them.</li>
				<li> Details of your visits to our Website including, but not limited to, traffic data, location data, weblogs and other communication data, whether this is required for our own billing purposes or otherwise and the resources that you access, these details will be collected by cookies.</li>
			</ol>
		</li>
		<li> For administration purposes only, you will be assigned a number on our database as you join the Website, and all data that you enter on the Website will be supplied linked to that number only, if it is shared in the context of collaborative research. Your name, email address and contact details will not be linked to that research data report and therefore not shared with any third parties. All research undertaken by or in collaboration with I Am Healthy uses only anonymous data.</li>
		</ol>
		</li>
		<p></p>
		<li>
		<strong>If you register with us or if you continue to use our Website, you agree to our use of these cookies.</strong>
		<ol>
			<li> We may collect information about your computer, including where available your IP address, operating system and browser type, for system administration. This is statistical data about our users' browsing actions and patterns, and does not identify any individual.</li>
			<li> The data that we collect from you may be transferred to, and stored at, a destination inside the European Economic Area ("EEA"). We will take all steps reasonably necessary to ensure that your data is transferred using a secure link.</li>
			<li> All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted using standard encryption technology in computer servers with limited access and in controlled facilities. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our Website, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</li>
			<li> We store your personal information for at least for the duration of any customer relationship we have with you, or as otherwise required by law (normally up to a maximum of 7 years for legal and tax reasons). </li>
			<li> Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our Website; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access and all data will be anonymous when it is stored.</li>
			<li> We never make your personal details available to third parties unless otherwise stated in this Privacy Policy. We use information held about you in the following ways:
			<ol>
				<li> To ensure that content from our Website is presented in the most effective manner for you and for your computer.</li>
				<li> To provide you with information or services that you request from us or which we feel may interest you, where you have consented to be contacted for such purposes.</li>
				<li> To allow you to participate in interactive features of our Service, when you choose to do so.</li>
				<li> To notify you about changes to our Service.</li>
			</ol>
			</li>
			<li> We may disclose your personal information to third parties:
				<ol>
					<li> In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.</li>
					<li> If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our terms of use or to protect the rights, property, or safety of I Am Healthy Limited, our subscribers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>
				</ol>
			</li>
			<li> We may disclose anonymous data to third parties including group companies, for research purposes. You have the right to ask us not to process your personal data for marketing or research purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by omitting to submit data on the relevant pages of the Website. You can also exercise the right at any time by contacting us at <a href="mailto:iamhealthy@asysinfo.com">iamhealthy@asysinfo.com</a> .</li>
			<li> Our Website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.</li>
			<li> Access to information
			<br>
			The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of Â£10 to meet our costs in providing you with details of the information we hold about you.</li>
			<li> Changes to our privacy policy
			<br>
			Any changes we may make to our Privacy Policy in the future will be posted on this page and, where appropriate, notified to you by e-mail.</li>
		</ol>
		</li>
		<p></p>
		<li><b> Our liability to you</b>
		<ol>
			<li> If we fail to comply with these Terms, we are responsible for loss or damage you suffer that is a foreseeable result of our breach of the Terms or our negligence, but we are not responsible for any loss or damage that is not foreseeable. Loss or damage is foreseeable if they were an obvious consequence of our breach or if they were contemplated by you and us at the time we entered into this contract.</li>
			<li> We supply the Services for domestic and private use, subject to clause 8.3 below. You agree not to use the Services for any business or re-sale purpose, and we have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity. </li>
			<li> You may use the Services for commercial purposes by buying subscriptions for a number of your employees should you wish to do so, provided that you give I Am Healthy credit for any techniques or information used. In such circumstances you or your business would not have the right to view any personal data of your staff, even in an anonymised form, without the prior and express consent of those employees.</li>
			<li> We do not exclude or limit in any way our liability for:
			<ol>
				<li> death or personal injury caused by our negligence;</li>
				<li> fraud or fraudulent misrepresentation.</li>
			</ol>
			</li>
		</ol>
		</li>
		<p></p>
		<li><b> Events Outside Our Control</b>
		<ol>
			<li> We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations under these Terms that is caused by an Event Outside Our Control.</li>
			<li> An Event Outside Our Control means any act or event beyond our reasonable control, including without limitation strikes, lock-outs or other industrial action by third parties, civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for war, fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster, or failure of public or private telecommunications networks.
			<ol>
				<li> If an Event Outside Our Control takes place that affects the performance of our obligations under these Terms:</li>
				<li> We will contact you as soon as reasonably possible to notify you; and</li>
				<li> Our obligations under these Terms will be suspended and the time for performance of our obligations will be extended for the duration of the Event Outside Our Control. Where the Event Outside Our Control affects our performance of Services to you, we will restart the Services as soon as reasonably possible after the Event Outside Our Control is over.</li>
			</ol>
			</li>
			<li> You may cancel the contract if an Event Outside Our Control takes place and continues for longer than six weeks in accordance with your cancellation rights in clause 10.</li>
		</ol>
		</li>
		<p></p>
		<li><b> Your cancellation rights</b>
			<ol>
				<li> Before your Subscription Period begins, you have the following rights as a consumer to cancel an Order, including where you choose to cancel because We are affected by an Event Outside Our Control or if we change these Terms under clause 3.1 to your material disadvantage:</li>
				<li> You may cancel any Order for subscription at any time before the start date for the Subscription Period by contacting us. We will confirm your cancellation in writing to you.</li>
				<li> If you cancel an Order under clause 10.1 and you have made any payment in advance for Services that have not been provided to you, we will refund these amounts to you.</li>
				<li> Once we have begun to provide the Services to you, you may cancel the contract for the Services within the first seven (7) days by providing us with notice in writing. Any payment you have made for Services beyond those seven days, that have not been provided, will be refunded to you.</li>
				<li> Once we have begun to provide the Services to you, you may cancel the contract for Services with immediate effect by giving us written notice if:</li>
				<li> We break this contract in any material way and we do not correct or fix the situation within 15 days of you asking us to in writing;</li>
				<li> We go into liquidation or a receiver or an administrator is appointed over our assets;</li>
				<li> We change these Terms under clause 3.1 to your material disadvantage;</li>
				<li> We are affected by an Event Outside Our Control for six weeks or more.</li>
			</ol>
		</li>
		<p></p>
		<li><b> Our cancellation rights</b>
			<ol>
				<li> If we have to cancel an Order before the Subscription Period begins:
					<ol>
						<li> We may have to cancel an Order before the start date for the Subscription Period, due to an Event Outside Our Control or the unavailability of key personnel or key materials without which we cannot provide the Services. We will promptly contact you if this happens.</li>
						<li> If we have to cancel an Order under clause 11.1(a) and you have made any payment in advance for Services that have not been provided to you, we will refund these amounts to you.</li>
					</ol>
				</li>
				<li> Once the Subscription Period has begun, we may cancel the contract for the Services at any time by providing you with at least [30] calendar days' notice in writing. If you have made any payment in advance for Services that have not been provided to you, we will refund these amounts to you.</li>
				<li> We may cancel the contract for Services at any time with immediate effect by giving you written notice if:
					<ol>
						<li> you do not pay us when you are supposed to as set out in clause 5.3. This does not affect our right to charge you interest; or</li>
						<li> you break the contract, or are in breach of our Terms of Website Use in any other material way and you do not correct or fix the situation within [15] days of us asking you to in writing.</li>
					</ol>
				</li>
			</ol>
		</li>
		<p></p>
		<li><b> How to Contact Us</b>
			<ol>
				<li> If you have any questions or if you have any complaints, please contact us by e-mailing us at <a href="mailto:iamhealthy@asysinfo.com">iamhealthy@asysinfo.com</a>.</li>
				<li> If you wish to contact us in writing, or if any clause in these Terms requires you to give us notice in writing (for example, to cancel the contract), you can send this to us by e-mail, by hand, or by pre-paid post to I Am Healthy (India) Limited at #5, Tirumala Nilaya, Room #17, 1st Main, 6th Cross, 1st Block Kormangala, Bangalore - 560034 AND/OR <a href="mailto:iamhealthy@asysinfo.com">iamhealthy@asysinfo.com</a>. We will confirm receipt of this by contacting you in writing. If we have to contact you or give you notice in writing, we will do so by e-mail, by hand, or by pre-paid post to the address you provide to us in the Order.</li>
			</ol>
			</li>
		<p></p>
		<li><b> Other important terms</b>
			<ol>
				<li> We may transfer our rights and obligations under these Terms to another organisation, and we will always notify you in writing if this happens, but this will not affect your rights or our obligations under these Terms.</li>
				<li> This Order is personal to you and you may only transfer your rights or your obligations under these Terms to another person if we agree in writing.</li>
				<li> This contract is between you and us. No other person shall have any rights to enforce any of its terms.</li>
				<li> Each of the paragraphs of these Terms operates separately. If any court or relevant authority decides that any of them are unlawful, the remaining paragraphs will remain in full force and effect.</li>
				<li> If we fail to insist that you perform any of your obligations under these Terms, or if we do not enforce our rights against you, or if we delay in doing so, that will not mean that we have waived our rights against you and will not mean that you do not have to comply with those obligations.</li>
				<li> These Terms are governed by English law. You and we both agree to submit to the non-exclusive jurisdiction of the English courts over any claim arising from, or related to, a visit to our Website although we retain the right to bring proceedings against you for breach of these conditions in your country of residence or any other relevant country.</li>
			</ol>
		</li>
	</ol>
	<br/>
	<h3> I Am Healthy Event Cancellation Policy</h3>
	<p> I Am Healthy is committed to providing high quality events to all its customers. We recognise that on occasion delegates will book a place on an event and subsequently find that they are unable to attend. In these situations we ask that customers notify us of their need to cancel an event booking as soon as possible. Any cancellation will be subject to the terms of this cancellation policy. </p>
	<p> Similarly, I Am Healthy may on occasion find it necessary to cancel an event and when this need arises we are committed to taking positive steps to alert you to the cancellation as soon as possible. </p>
	<p> The information below outlines the action to be taken in case of cancellation:</p>
	<ol>
		<li><b> Cancellation by delegates:</b>
			<ol>
				<li> If you need to cancel your booking(s) for an event, our cancellation policy is as follows: </li>
				<li> Please send cancellation details to <a href="mailto:iamhealthy@asysinfo.com">iamhealthy@asysinfo.com</a> or call +91-9663627986. </li>
				<li> If you have booked and are unable to attend please provide at least 48 hours' notice, so that the place can be made available to another applicant on the waiting list. If you notify I Am Healthy of your cancellation up to 48 hours prior to the event, we will provide a full refund or offer a transfer of credit for another event â€“ there will be no charges. </li>
				<li> For any cancellations received by I Am Healthy within 48 hours of the event we will not provide a refund of the fee. </li>
				<li> Where you have booked a place at an event and do not attend without notifying I Am Healthy of your cancellation we will not provide a refund of the fee. </li>
				<li> Please inform us if you wish to send a replacement delegate, including their requirements (e.g. diet), so we can ensure that we offer them the best possible service at the event. </li>
			</ol>
		</li>
		<p></p>
		<li><b> Cancellation of an event by I Am Healthy: </b>
			<ol>
				<li> I Am Healthy is committed to offering high quality events that meet the needs of delegates and will always aim to run events once delegates have booked onto them. Occasionally circumstances will arise which result in the need to cancel an event. When this is the case I Am Healthy will take active and positive steps to inform you as soon as possible by phone and/or email. I Am Healthy reserves the right to modify or cancel any event if unforeseen circumstances arise but we will do our utmost to avoid doing so. </li>
				<li> Where I Am Healthy cancels an event that you have paid to attend, we will offer you the option of a full refund of that payment or a credit transfer for a future event. For the avoidance of doubt, I Am Healthy will not be liable for any other incidental costs incurred such as travel, accommodation, or loss of earnings. </li>
			</ol>
		</li>
	</ol>
	<br/>
	<p></p>
</div>
@endsection