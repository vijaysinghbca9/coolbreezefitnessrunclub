@extends('layouts.master')

@section('content')
<div class="headerform">
    <div class="container" style="padding-top: 200px">
    	<div class="table center">
        	<div class="header-text">
                <h2 class="blue">{{ __('Register') }}</h2><br/><br/>
                <form action="{{ url('/consultation/register/'.$type) }}" method="POST">
                	@csrf
                	<div class="form-group row">
                		<label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" name="firstname" value="{{ old('firstname') }}" required autofocus />
                    		@if ($errors->has('firstname'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('firstname') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>					
                	<div class="form-group row">
                		<label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
                		<div class="col-md-7">
                			<input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" name="lastname" value="{{ old('lastname') }}" required autofocus />
                    		@if ($errors->has('lastname'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('lastname') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                		<div class="col-md-7">
                			<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" required />
                    		@if ($errors->has('email'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('email') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>
                		<div class="col-md-3">
                    		<input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" id="mobile" name="mobile" value="{{ old('mobile') }}" required autofocus />
                    		@if ($errors->has('mobile'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('mobile') }}</strong>
                    			</span>
                    		@endif
                		</div>
                		<label for="sex" class="col-md-1 col-form-label text-md-right">{{ __('Sex') }}</label>
                		<div class="col-md-3">
                			<input type="radio" name="sex" value="Male"  checked>&nbsp;&nbsp;{{ __('Male') }}&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="sex" value="Female">&nbsp;&nbsp;{{ __('Female') }}
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                		<div class="col-md-3">
                    		<input type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" name="city" value="{{ old('city') }}" required autofocus />
                    		@if ($errors->has('city'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('city') }}</strong>
                    			</span>
                    		@endif
                		</div>
                		<label for="age" class="col-md-1 col-form-label text-md-right">{{ __('Age') }}</label>
                		<div class="col-md-3">
                			<input type="text" class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" id="age" name="age" value="{{ old('age') }}" autofocus />
                			@if ($errors->has('age'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('age') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="howknowus" class="col-md-4 col-form-label text-md-right">{{ __('How did you come to know about us?') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('howknowus') ? ' is-invalid' : '' }}" id="howknowus" name="howknowus" value="{{ old('howknowus') }}" />
                    		@if ($errors->has('howknowus'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('howknowus') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="healthissue" class="col-md-4 col-form-label text-md-right">{{ __('Are you facing any health issue?') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('healthissue') ? ' is-invalid' : '' }}" id="healthissue" name="healthissue" value="{{ old('healthissue') }}" multiple="multiple" autofocus />
                    		@if ($errors->has('healthissue'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('healthissue') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="skinissue" class="col-md-4 col-form-label text-md-right">{{ __('Are you facing any skin issue?') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('skinissue') ? ' is-invalid' : '' }}" id="skinissue" name="skinissue" value="{{ old('skinissue') }}" multiple="multiple" autofocus />
                    		@if ($errors->has('skinissue'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('skinissue') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="isundermedication" class="col-md-4 col-form-label text-md-right">{{ __('Are you under some kind of medication?') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('isundermedication') ? ' is-invalid' : '' }}" id="isundermedication" name="isundermedication" value="{{ old('isundermedication') }}" multiple="multiple" autofocus />
                    		@if ($errors->has('isundermedication'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('isundermedication') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="howlongthisissue" class="col-md-4 col-form-label text-md-right">{{ __('Since how long you are facing this problem?') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('howlongthisissue') ? ' is-invalid' : '' }}" id="howlongthisissue" name="howlongthisissue" value="{{ old('howlongthisissue') }}" multiple="multiple" autofocus />
                    		@if ($errors->has('howlongthisissue'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('howlongthisissue') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="forwhom" class="col-md-4 col-form-label text-md-right">{{ __('For whom are you looking for consultation?') }}</label>
                		<div class="col-md-7">
                    		<input type="text" class="form-control{{ $errors->has('forwhom') ? ' is-invalid' : '' }}" id="forwhom" name="forwhom" value="{{ old('forwhom') }}" multiple="multiple" autofocus />
                    		@if ($errors->has('forwhom'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('forwhom') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="inviteforevents" class="col-md-4 col-form-label text-md-right">{{ __('We organize events for health awareness and prevention. Would you like to be invited?') }}</label>
                		<div class="col-md-3">
                			<input type="radio" name="inviteforevents" value="Yes"  checked>&nbsp;&nbsp;{{ __('Yes') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="inviteforevents" value="No">&nbsp;&nbsp;{{ __('No') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="inviteforevents" value="May be">&nbsp;&nbsp;{{ __('May be') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    		@if ($errors->has('inviteforevents'))
                    			<span class="invalid-feedback">
                    				<strong>{{ $errors->first('inviteforevents') }}</strong>
                    			</span>
                    		@endif
                		</div>
                	</div>
                	<div class="form-group row">
                		<label for="squaredOne" class="col-md-4 col-form-label text-md-right"></label>
                		<div class="checkbox-holder text-left">
                			<div class="checkbox col-md-6">
                				<input type="checkbox" value="None" id="squaredOne" name="check" />
                				<label for="squaredOne"><a href="{{ url('/termsandconditions') }}"><span>I Agree to the <strong>Terms &amp; Conditions</strong></span></a></label>
                			</div>
                		</div>
                			@if ($errors->has('check'))
                				<span class="invalid-feedback">
                					<strong>Please accept terms & conditions</strong>
                				</span>
                			@endif
                	</div>
                	<button type="submit" class="btn blue-bg" id="btnregister" name="submit">Book Now</button>
                </form>
        	</div>
    	</div>
     </div>
</div>
@endsection