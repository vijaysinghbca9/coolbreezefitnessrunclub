<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () { return view('index'); });
Route::get('/termsandconditions', function() { return view('termsandconditions'); });
Route::get('/account/activation/{id}/{hash}', ['uses'=>'Auth\RegisterController@activate'])->where('hash', '[A-Za-z0-9_/$.-]+');
Route::get('/account/status', function(){ return view('status'); });
Route::get('/consultation/status', ['uses'=>'ConsultationController@status']);
Route::get('/consultation/form/{type}', ['uses'=>'ConsultationController@showForm'])->name('Consultation Form');
Route::post('/consultation/register/{type}', ['uses'=>'ConsultationController@register']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@index');
