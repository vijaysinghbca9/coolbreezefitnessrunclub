<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 256);
            $table->string('lastname', 256);
            $table->string('mobile');
            $table->string('email', 50)->index();
            $table->string('city', 256);
            $table->string('sex', 20);
            $table->tinyInteger('age')->nullable();
            $table->string('howknowus', 256)->nullable();
            $table->string('healthissue', 512)->nullable();
            $table->string('skinissue', 512)->nullable();
            $table->string('isundermedication', 256)->nullable();
            $table->string('howlongthisissue', 256)->nullable();
            $table->string('inviteforevents', 50)->nullable();
            $table->string('forwhom', 256)->nullable();
            $table->boolean('tc_agree', 1);
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation');
    }
}
